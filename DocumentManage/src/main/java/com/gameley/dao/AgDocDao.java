package com.gameley.dao;

import com.gameley.entity.AgDoc;
import com.gameley.common.mapper.SuperMapper;

/**
 *
 * 
 *
 * @author WWMXD
 * @email 309980030@qq.com
 * @date 2018-01-30 14:46:58
 */
public interface AgDocDao extends SuperMapper<AgDoc> {

}